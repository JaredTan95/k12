# K12 在线教育
## 开发环境（建议）：
* Java 1.8+
* IDEA (导入CodeStyle:scripts/codestyle/intellij-java-google-style.xml)

## 项目模块说明：
> 每个模块均独立开发。
* docs:放置文档以及说明图。
* scripts:放置脚本。
* K12-OAuth2:基于OAuth2实现的第三方应用登陆。
* K12-Course:课程服务。
* ......

## 准备工作(Preconditions) :
* 配置resources/application.yml文件中数据库连接,并确保名为K12-CourseDB的数据库存在,程序会自动创建实体表结构。
* ......

## API快速预览
* 每个服务均应配置接口文档：
> e.g: 课程服务接口访问方式：
> * Json: http://ip:port/v1/api-docs
> * UI: http://ip:port/docs.html


# Archi
> href:https://piotrminkowski.wordpress.com/2017/02/22/microservices-security-with-oauth2/








ip:140.143.100.23
