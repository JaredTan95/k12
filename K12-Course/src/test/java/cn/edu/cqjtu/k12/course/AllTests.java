package cn.edu.cqjtu.k12.course;

import cn.edu.cqjtu.k12.course.repository.CategoryRepositoryTest;
import cn.edu.cqjtu.k12.course.service.CategoryServiceTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
    CategoryRepositoryTest.class,
    CategoryServiceTest.class
})
public class AllTests {

}
