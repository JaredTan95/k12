package cn.edu.cqjtu.k12.course.service;

import cn.edu.cqjtu.k12.course.AbstractIntegrationTest;
import cn.edu.cqjtu.k12.course.domain.entity.Category;
import cn.edu.cqjtu.k12.course.repository.CategoryRepository;
import cn.edu.cqjtu.k12.course.service.CategoryService.DealCourseChoice;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class CategoryServiceTest extends AbstractIntegrationTest {

  @Autowired
  private CategoryService categoryService;

  @Autowired
  private CategoryRepository categoryRepository;

  @Test
  public void testSave() {
    String title = "office";
    Category entity = new Category();
    entity.setTitle(title);
    Assert.assertEquals(title, categoryService.save(entity).getTitle());
  }

  @Test
  public void testDeleteWithDeleteCourse() {
    long id = 76;
    Category category = new Category();
    category.setId(id);

    Assert.assertEquals(21, categoryRepository.findAll().size());

    categoryService.deleteCategory(category, DealCourseChoice.DELETE);

    Assert.assertEquals(17, categoryRepository.findAll().size());
  }
}
