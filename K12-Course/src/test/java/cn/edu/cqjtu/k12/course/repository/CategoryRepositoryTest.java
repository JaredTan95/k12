package cn.edu.cqjtu.k12.course.repository;

import cn.edu.cqjtu.k12.course.AbstractIntegrationTest;
import cn.edu.cqjtu.k12.course.domain.converter.CategoryConverter;
import cn.edu.cqjtu.k12.course.domain.entity.Category;
import cn.edu.cqjtu.k12.course.domain.dto.CategoryDTO;
import cn.edu.cqjtu.k12.course.foundation.utils.TreeBuilderUtils;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;

public class CategoryRepositoryTest extends AbstractIntegrationTest {

  @Autowired
  private CategoryRepository categoryRepository;

  @Autowired
  private CategoryConverter categoryConverter;

  @Test
  //避免受到data.sql数据影响,先先清理。
  @Sql(scripts = "/sql/clean.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
  @Sql(scripts = "/sql/repository/Category-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
  @Sql(scripts = "/sql/clean.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
  public void testFindAll() {
    List<Category> lists = categoryRepository.findAll();
    Assert.assertEquals(7, lists.size());
    categoryRepository.delete(1000l);
    Assert.assertEquals(null, categoryRepository.findOne(1000l));
    Assert.assertEquals(null, categoryRepository.findOne(1113l));
    for (Category item : categoryRepository.findAll()) {
      System.out.println(item.getId() + "," + item.getTitle());
    }
  }

  @Test
  @Sql(scripts = "/sql/clean.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
  @Sql(scripts = "/sql/repository/Category-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
  @Sql(scripts = "/sql/clean.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
  public void testFindAllByParentId() {
    Assert.assertEquals(3, categoryRepository.findAllByParentId(60).size());
    categoryRepository.delete(1004l);
    Assert.assertEquals(2, categoryRepository.findAllByParentId(60).size());
  }

  @Test
  @Sql(scripts = "/sql/clean.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
  @Sql(scripts = "/sql/repository/Category-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
  @Sql(scripts = "/sql/clean.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
  public void testFindByTitle() {
    Assert.assertEquals(1003, categoryRepository.findByTitle("编程语言").getId());
  }

  @Test
  //@Sql(scripts = "/sql/clean.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
  //@Sql(scripts = "/sql/repository/Category-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
  @Sql(scripts = "/sql/clean.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
  public void testBuildCategoriesTree() {
    List<CategoryDTO> categoryDTOS = categoryRepository.findAll().stream()
        .map(item -> categoryConverter.convert(item))
        .collect(Collectors.toList());
    categoryDTOS = new TreeBuilderUtils(categoryDTOS).buildTree();
    Assert.assertEquals(6, categoryDTOS.size());
  }
}
