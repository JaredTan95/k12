package cn.edu.cqjtu.k12.course;

import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public abstract class AbstractUnitTest {

}
