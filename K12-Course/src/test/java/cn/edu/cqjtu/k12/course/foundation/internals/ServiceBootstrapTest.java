package cn.edu.cqjtu.k12.course.foundation.internals;

import static org.junit.Assert.assertTrue;

import cn.edu.cqjtu.k12.course.foundation.spi.FileUploadProvider;
import com.qiniu.common.Zone;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import org.junit.Test;

public class ServiceBootstrapTest {


  @Test
  public void loadFirstSuccessfully() {
    FileUploadProvider service = ServiceBootstrap.loadFirst(FileUploadProvider.class);
    assertTrue(service instanceof QiniuFileUploadProvider);

  }

  @Test
  public void uploadFile() throws Exception {
    FileUploadProvider service = ServiceBootstrap.loadFirst(FileUploadProvider.class);

    File file = new File("/Users/tanjian/gitprojects/K12/pom.xml");

    BufferedReader br = new BufferedReader(new FileReader(file));

    String st;
    while ((st = br.readLine()) != null) {
      System.out.println(st);
    }

    System.out.println("file.getPath():" + file.getPath());
    QiniuUploaderProperty property = new QiniuUploaderProperty();
    property.setFileName(file.getName());
    property.setFilePath(file.getPath());
    property.setBucket("k12-course");
    property.setZone(Zone.zone0());
    service.uploadByFile(property, file.getPath());

  }


}
