package cn.edu.cqjtu.k12.course.domain.dto;

public class UploadResponseDTO {

    public int statusCode;
    public String error;
    public double duration;
    public String address;
    public String key;
    public String hash;
    public static final int InvalidArgument = -4;
    public static final int InvalidFile = -3;
    public static final int Cancelled = -2;
    public static final int NetworkError = -1;

    public UploadResponseDTO() {
    }

    public UploadResponseDTO(int statusCode, String error, double duration, String address) {
        this.statusCode = statusCode;
        this.error = error;
        this.duration = duration;
        this.address = address;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public UploadResponseDTO setStatusCode(int statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    public String getError() {
        return error;
    }

    public UploadResponseDTO setError(String error) {
        this.error = error;
        return this;
    }

    public double getDuration() {
        return duration;
    }

    public UploadResponseDTO setDuration(double duration) {
        this.duration = duration;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public UploadResponseDTO setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getKey() {
        return key;
    }

    public UploadResponseDTO setKey(String key) {
        this.key = key;
        return this;
    }

    public String getHash() {
        return hash;
    }

    public UploadResponseDTO setHash(String hash) {
        this.hash = hash;
        return this;
    }
}
