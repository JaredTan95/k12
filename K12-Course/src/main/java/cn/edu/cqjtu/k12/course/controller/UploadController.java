package cn.edu.cqjtu.k12.course.controller;

import cn.edu.cqjtu.k12.course.domain.dto.UploadResponseDTO;
import cn.edu.cqjtu.k12.course.exception.BadRequestException;
import cn.edu.cqjtu.k12.course.foundation.internals.QiniuUploaderProperty;
import cn.edu.cqjtu.k12.course.foundation.internals.ServiceBootstrap;
import cn.edu.cqjtu.k12.course.foundation.spi.FileUploadProvider;
import java.io.IOException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/upload")
public class UploadController {

  private static final Logger logger = LoggerFactory.getLogger(UploadController.class);

  @Value("${qiniu.key.courseBucket:k12-course}")
  String courseBucket;

  @Value("${qiniu.key.domainOfCourseBucket}")
  String domainOfCourseBkt;

  @Value("${qiniu.key.videoBucket:k12-video}")
  String videoBucket;

  @Value("${qiniu.key.domainOfVideoBucket}")
  String domainOfVideoBkt;

  @PostMapping("/course")
  public UploadResponseDTO singleCourseFileUpload(@RequestParam("file") MultipartFile file) {

    if (file.isEmpty()) {
      throw new BadRequestException("File must not be null,Please select a file to upload.");
    }

    UploadResponseDTO response = new UploadResponseDTO();
    try {
      QiniuUploaderProperty property = new QiniuUploaderProperty();
      property.setFileName(file.getOriginalFilename());
      property.setBucket(courseBucket);
      FileUploadProvider service = ServiceBootstrap.loadFirst(FileUploadProvider.class);

      //trying upload to qiniu.
      byte[] bytes = file.getBytes();
      logger.info("Start uploading course data:{}.", property.getFileName());
      response = service.uploadByBytes(property, bytes);
      response
          .setAddress(StringUtils.join(new String[]{domainOfCourseBkt, response.getKey()}, "/"));
      logger.info("Upload course data completed:{}.", property.getFileName());

    } catch (IOException e) {
      logger.error("Upload course file exception:{}", e.getCause().getMessage());
      e.printStackTrace();
    }
    return response;
  }

  @PostMapping("/video")
  public UploadResponseDTO singleVideoFileUpload(@RequestParam("file") MultipartFile file) {

    if (file.isEmpty()) {
      throw new BadRequestException("File must not be null,Please select a file to upload.");
    }
    UploadResponseDTO response = new UploadResponseDTO();
    try {
      QiniuUploaderProperty property = new QiniuUploaderProperty();
      property.setFileName(file.getOriginalFilename());
      property.setBucket(videoBucket);
      FileUploadProvider service = ServiceBootstrap.loadFirst(FileUploadProvider.class);
      byte[] bytes = file.getBytes();
      logger.info("Start uploading video data:{}.", property.getFileName());
      response = service.uploadByBytes(property, bytes);
      response
          .setAddress(StringUtils.join(new String[]{domainOfVideoBkt, response.getKey()}, "/"));
      logger.info("Upload video data completed:{}.", property.getFileName());
    } catch (IOException e) {
      logger.error("Upload video file exception:{}", e.getCause().getMessage());
      e.printStackTrace();
    }
    return response;
  }
}
