package cn.edu.cqjtu.k12.course.domain.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "Category")
@SQLDelete(sql = "UPDATE Category SET Is_Deleted = 1 WHERE Id = ?")
@Where(clause = "Is_Deleted = 0")
public class Category extends BaseEntity implements Comparable<Category> {

    @Column(nullable = false)
    private String title;

    @Column(columnDefinition = "Int default '0'")
    private long parentId;

    @Column
    private String description;

    public Category() {
    }

    public Category(long id, boolean isDeleted,
                    String title, long parentId, String description,
                    String dataChangeCreatedBy,
                    Date dataChangeCreatedTime, String dataChangeLastModifiedBy,
                    Date dataChangeLastModifiedTime) {
        this.id = id;
        this.isDeleted = isDeleted;
        this.title = title;
        this.parentId = parentId;
        this.description = description;
        this.dataChangeCreatedBy = dataChangeCreatedBy;
        this.dataChangeCreatedTime = dataChangeCreatedTime;
        this.dataChangeLastModifiedBy = dataChangeLastModifiedBy;
        this.dataChangeLastModifiedTime = dataChangeLastModifiedTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int compareTo(Category o) {
        if (o == null || getId() > o.getId()) {
            return 1;
        }

        if (getId() == o.getId()) {
            return 0;
        }

        return -1;
    }
}
