package cn.edu.cqjtu.k12.course.service;

import cn.edu.cqjtu.k12.course.domain.converter.CategoryConverter;
import cn.edu.cqjtu.k12.course.domain.entity.CategoriesCourse;
import cn.edu.cqjtu.k12.course.domain.entity.Category;
import cn.edu.cqjtu.k12.course.domain.dto.CategoryDTO;
import cn.edu.cqjtu.k12.course.exception.BadRequestException;
import cn.edu.cqjtu.k12.course.exception.NotFoundException;
import cn.edu.cqjtu.k12.course.exception.ServiceException;
import cn.edu.cqjtu.k12.course.repository.CategoryRepository;
import cn.edu.cqjtu.k12.course.foundation.utils.TreeBuilderUtils;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CategoryService {

  @Autowired
  private CategoryRepository categoryRepository;

  @Autowired
  private CourseService courseService;

  @Autowired
  private CategoriesCourseService categoriesCourseService;

  @Autowired
  private CategoryConverter categoryConverter;

  /**
   * get a categories-tree.
   */
  public List<CategoryDTO> findAll() {
    List<CategoryDTO> categoryDTOS = categoryRepository.findAll().stream()
        .map(item -> categoryConverter.convert(item))
        .collect(Collectors.toList());
    return new TreeBuilderUtils<>(categoryDTOS).buildTree();
  }

  @Transactional
  public CategoryDTO save(Category entity) {
    if (!isCategoryTitleUnique(entity.getTitle())) {
      throw new ServiceException("Category's title is not unique.");
    }
    entity.setId(0);//protection
    return categoryConverter.convert(categoryRepository.save(entity));
  }

  /**
   * make sure that Category's title is unique.
   */
  public boolean isCategoryTitleUnique(String title) {
    Objects.requireNonNull(title, "Category's title must not null.");
    return Objects.isNull(categoryRepository.findByTitle(title));
  }

  /**
   * judging by category id is exists to update category info.
   */
  @Transactional
  public CategoryDTO updateById(Category entity) {
    Category category = categoryRepository.findOne(entity.getId());
    if (Objects.isNull(category)) {
      throw new BadRequestException(
          String.format("Category id not exists. id = %s", entity.getId()));
    }
    category.setTitle(entity.getTitle());
    category.setDescription(entity.getDescription());
    category.setParentId(entity.getParentId());
    return categoryConverter.convert(categoryRepository.save(category));
  }

  /**
   * judging by category id is exists to delete category info.
   */
  @Transactional
  public List<CategoryDTO> deleteCategory(Category entity, DealCourseChoice method) {
    long id = entity.getId();
    Category category = categoryRepository.findOne(id);
    if (Objects.isNull(category)) {
      throw new BadRequestException(
          String.format("Category id not exists. id = %s", id));
    }
    List<CategoriesCourse> categoriesCourses = categoriesCourseService.findByCategory(category);
    switch (method) {
      //`force` to delete the related course.
      case DELETE: {
        courseService.deleteAllByCategoryId(category.getId());
      }
      break;
      // make those related course's category to 'non-category'.
      case OTHERS: {
        List<Long> courseIds = categoriesCourses.stream()
            .map(CategoriesCourse::getCourseId).collect(Collectors.toList());
        courseService.modifyCategoryToOthers(courseIds);
      }
      break;
      default: {
        //check there is course related to category?
        if (categoriesCourseService.findCourseByCategory(entity).size() > 0) {
          throw new BadRequestException(String
              .format("Can not delete category[%d,%s] because there is course related to it.",
                  category.getId(), category.getTitle()));
        }
      }
    }

    Set<Long> ids = categoryRepository.findAllByParentId(id).stream()
        .map(Category::getId)
        .collect(Collectors.toSet());

    List<Category> deletedLists = categoryRepository.deleteAllByIdIn(ids);
    if (categoryRepository.deleteById(id) > 0) {
      deletedLists.add(category);
    }
    for (CategoriesCourse item : categoriesCourses) {
      categoriesCourseService.delete(item);
    }
    return deletedLists.stream().map(item -> categoryConverter.convert(item))
        .collect(Collectors.toList());
  }


  public CategoryDTO getOne(long id) {
    Category category = categoryRepository.findOne(id);
    if (Objects.isNull(category)) {
      throw new NotFoundException(String.format("Category id not exists.id = %s", id));
    }
    return categoryConverter.convert(category);
  }

  public enum DealCourseChoice {
    DEFAULT, DELETE, OTHERS;
  }
}
