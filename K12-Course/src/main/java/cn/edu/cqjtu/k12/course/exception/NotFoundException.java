package cn.edu.cqjtu.k12.course.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends AbstractK12HttpException {


  public NotFoundException(String str) {
    super(str);
    setHttpStatus(HttpStatus.NOT_FOUND);
  }
}
