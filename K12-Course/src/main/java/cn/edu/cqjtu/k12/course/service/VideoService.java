package cn.edu.cqjtu.k12.course.service;

import cn.edu.cqjtu.k12.course.domain.entity.Course;
import cn.edu.cqjtu.k12.course.domain.entity.Video;
import cn.edu.cqjtu.k12.course.exception.BadRequestException;
import cn.edu.cqjtu.k12.course.exception.ServiceException;
import cn.edu.cqjtu.k12.course.repository.VideoRepository;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VideoService {

  @Autowired
  private VideoRepository videoRepository;

  /**
   * add a new video.
   */
  public Video save(Video video) {
    if (!isVideoTitleUnique(video.getTitle())) {
      throw new ServiceException("Video's title is not unique.");
    }
    video.setId(0);//protection
    return videoRepository.save(video);
  }

  /**
   * get videos by course.
   */
  public List<Video> getByCourse(Course course) {
    return videoRepository.findAllByCourseId(course.getId());
  }

  /**
   * get previous video.
   */
  public Video getPreVideo(long id) {
    long parentId = videoRepository.findOne(id).getParentId();
    return videoRepository.findOne(parentId);
  }

  /**
   * get next video.
   */
  public Video getNextVideo(long id) {
    return videoRepository.findByParentId(id);
  }

  /**
   * make sure that Video's title is unique.
   */
  public boolean isVideoTitleUnique(String title) {
    return Objects.isNull(videoRepository.findAllByTitle(title));
  }

  /**
   * update one video info.
   */
  public Video updateOne(Video video) {
    Video entity = videoRepository.findOne(video.getId());
    if (Objects.isNull(entity)) {
      throw new BadRequestException(
          String.format("Video id not exists. id = %s\"", entity.getId()));
    }
    entity.setTitle(video.getTitle());
    entity.setDescription(video.getDescription());
    entity.setUrl(video.getUrl());
    entity.setCover(video.getCover());
    entity.setDuration(video.getDuration());
    entity.setCategoryId(video.getCategoryId());
    entity.setCourseId(video.getCourseId());
    entity.setParentId(video.getParentId());
    return videoRepository.save(entity);
  }

  public Video getOne(long id) {
    return videoRepository.findOne(id);
  }

  public void deleteOne(long id) {
    videoRepository.delete(id);
  }
}
