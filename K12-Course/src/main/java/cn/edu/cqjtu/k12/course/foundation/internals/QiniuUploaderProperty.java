package cn.edu.cqjtu.k12.course.foundation.internals;

import com.qiniu.common.Zone;
import org.springframework.beans.factory.annotation.Value;

public class QiniuUploaderProperty {

  @Value("${qiniu.key.accessKey:5c8XqP_Hene9ZnSCs6_akY6U0E-FBW84s1cQoVLf}")
  String accessKey = "5c8XqP_Hene9ZnSCs6_akY6U0E-FBW84s1cQoVLf";

  @Value("${qiniu.key.secretKey:7Yb7n4i8BMFgzZ2SgiC8-JXUpPpr3QfxM0i1HUnz}")
  String secretKey = "7Yb7n4i8BMFgzZ2SgiC8-JXUpPpr3QfxM0i1HUnz";

  @Value("${qiniu.key.domainOfBucket}")
  String domainOfBucket;

  @Value("${qiniu.key.courseBucket:k12-course}")
  String courseBucket;

  @Value("${qiniu.key.videoBucket:k12-video}")
  String videoBucket;

  String bucket;

  private Zone zone;

  private String fileName;

  private String filePath;

  public QiniuUploaderProperty() {
  }

  public QiniuUploaderProperty(Zone zone, String fileName, String filePath, String bucket) {
    this.zone = zone;
    this.fileName = fileName;
    this.filePath = filePath;
  }

  public String getAccessKey() {
    return accessKey;
  }

  public void setAccessKey(String accessKey) {
    this.accessKey = accessKey;
  }

  public String getSecretKey() {
    return secretKey;
  }

  public void setSecretKey(String secretKey) {
    this.secretKey = secretKey;
  }

  public String getDomainOfBucket() {
    return domainOfBucket;
  }

  public void setDomainOfBucket(String domainOfBucket) {
    this.domainOfBucket = domainOfBucket;
  }

  public String getCourseBucket() {
    return courseBucket;
  }

  public void setCourseBucket(String courseBucket) {
    this.courseBucket = courseBucket;
  }

  public String getVideoBucket() {
    return videoBucket;
  }

  public void setVideoBucket(String videoBucket) {
    this.videoBucket = videoBucket;
  }

  public Zone getZone() {
    return zone;
  }

  public void setZone(Zone zone) {
    this.zone = zone;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public String getFilePath() {
    return filePath;
  }

  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }

  public String getBucket() {
    return bucket;
  }

  public void setBucket(String bucket) {
    this.bucket = bucket;
  }
}
