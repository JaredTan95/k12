package cn.edu.cqjtu.k12.course.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

/**
 * many to many, one course should have many category attributes and one category includes many
 * courses.
 */
@Entity
@Table(name = "CategoriesCourse")
@SQLDelete(sql = "UPDATE Categories_Course SET Is_Deleted = 1 WHERE Id = ?")
@Where(clause = "Is_Deleted = 0")
public class CategoriesCourse extends BaseEntity {

    @Column(nullable = false, columnDefinition = "Int default '0'")
    private long categoryId;

    @Column(nullable = false, columnDefinition = "Int default '0'")
    private long courseId;

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }
}
