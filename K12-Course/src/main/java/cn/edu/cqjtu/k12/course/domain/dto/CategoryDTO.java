package cn.edu.cqjtu.k12.course.domain.dto;

import cn.edu.cqjtu.k12.course.foundation.utils.Node;

import java.util.Date;
import java.util.List;

/**
 * Category domain transforms object.
 */
public class CategoryDTO extends Node {

    private long id;

    protected boolean isDeleted = false;

    private String title;

    private long parentId;

    private String description;

    private List<CategoryDTO> categoryDTOS;

    private String dataChangeCreatedBy;

    private Date dataChangeCreatedTime;

    private String dataChangeLastModifiedBy;

    private Date dataChangeLastModifiedTime;

    public CategoryDTO() {

    }

    public CategoryDTO(long id, long parentId, String title) {
        this.id = id;
        this.title = title;
        this.parentId = parentId;
    }

    public CategoryDTO(long id, boolean isDeleted, String title, long parentId,
                       String description, String dataChangeCreatedBy,
                       Date dataChangeCreatedTime, String dataChangeLastModifiedBy,
                       Date dataChangeLastModifiedTime) {
        this.id = id;
        this.isDeleted = isDeleted;
        this.title = title;
        this.parentId = parentId;
        this.description = description;
        this.dataChangeCreatedBy = dataChangeCreatedBy;
        this.dataChangeCreatedTime = dataChangeCreatedTime;
        this.dataChangeLastModifiedBy = dataChangeLastModifiedBy;
        this.dataChangeLastModifiedTime = dataChangeLastModifiedTime;
    }

    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CategoryDTO> getCategoryDTOS() {
        return categoryDTOS;
    }

    public void setCategoryDTOS(List<CategoryDTO> categoryDTOS) {
        this.categoryDTOS = categoryDTOS;
    }

    public String getDataChangeCreatedBy() {
        return dataChangeCreatedBy;
    }

    public void setDataChangeCreatedBy(String dataChangeCreatedBy) {
        this.dataChangeCreatedBy = dataChangeCreatedBy;
    }

    public Date getDataChangeCreatedTime() {
        return dataChangeCreatedTime;
    }

    public void setDataChangeCreatedTime(Date dataChangeCreatedTime) {
        this.dataChangeCreatedTime = dataChangeCreatedTime;
    }

    public String getDataChangeLastModifiedBy() {
        return dataChangeLastModifiedBy;
    }

    public void setDataChangeLastModifiedBy(String dataChangeLastModifiedBy) {
        this.dataChangeLastModifiedBy = dataChangeLastModifiedBy;
    }

    public Date getDataChangeLastModifiedTime() {
        return dataChangeLastModifiedTime;
    }

    public void setDataChangeLastModifiedTime(Date dataChangeLastModifiedTime) {
        this.dataChangeLastModifiedTime = dataChangeLastModifiedTime;
    }
}
