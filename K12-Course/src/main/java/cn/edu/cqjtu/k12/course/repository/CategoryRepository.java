package cn.edu.cqjtu.k12.course.repository;

import cn.edu.cqjtu.k12.course.domain.entity.Category;
import java.util.List;
import java.util.Set;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CategoryRepository extends PagingAndSortingRepository<Category, Long> {

  List<Category> findAll();

  List<Category> findAllByParentId(long parentId);

  Category findByTitle(String title);

  int deleteById(long id);

  List<Category> deleteAllByIdIn(Set<Long> ids);
}
