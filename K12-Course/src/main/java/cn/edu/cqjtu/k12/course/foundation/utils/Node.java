package cn.edu.cqjtu.k12.course.foundation.utils;

import java.io.Serializable;
import java.util.List;

public class Node implements Serializable {

  private long id;
  private long parentId;
  //child nodes.
  private List<Node> nodes;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getParentId() {
    return parentId;
  }

  public void setParentId(long parentId) {
    this.parentId = parentId;
  }

  public List<Node> getNodes() {
    return nodes;
  }

  public void setNodes(List<Node> nodes) {
    this.nodes = nodes;
  }
}
