package cn.edu.cqjtu.k12.course.service;

import cn.edu.cqjtu.k12.course.domain.entity.CategoriesCourse;
import cn.edu.cqjtu.k12.course.domain.entity.Category;
import cn.edu.cqjtu.k12.course.domain.entity.Course;
import cn.edu.cqjtu.k12.course.repository.CategoriesCourseRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoriesCourseService {

  @Autowired
  private CategoriesCourseRepository categoriesCourseRepository;

  @Autowired
  private CourseService courseService;

  public CategoriesCourse save(CategoriesCourse categoriesCourse) {
    return categoriesCourseRepository.save(categoriesCourse);
  }

  /**
   * getting lists of course related to the category.
   */
  public List<Long> findCourseByCategory(Category category) {
    return categoriesCourseRepository.findAllByCategoryId(category.getId()).stream()
        .map(CategoriesCourse::getCourseId).collect(Collectors.toList());
  }

  /**
   * getting categories related to the course.
   */
  public List<Long> findCategoriesByCourse(Course course) {
    return categoriesCourseRepository.findAllByCourseId(course.getId()).stream()
        .map(CategoriesCourse::getCategoryId).collect(Collectors.toList());
  }

  public void delete(CategoriesCourse categoriesCourse) {
    categoriesCourseRepository.delete(categoriesCourse);
  }

  public List<CategoriesCourse> findByCategory(Category category) {
    return categoriesCourseRepository.findAllByCategoryId(category.getId());
  }

  public List<CategoriesCourse> findByCourse(Course course) {
    return categoriesCourseRepository.findAllByCourseId(course.getId());
  }
}
