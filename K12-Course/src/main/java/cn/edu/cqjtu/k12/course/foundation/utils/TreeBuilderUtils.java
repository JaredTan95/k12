package cn.edu.cqjtu.k12.course.foundation.utils;

import com.google.common.collect.Lists;
import java.util.List;

/**
 * utils for build categories tree.
 */
public class TreeBuilderUtils<T extends Node> {

  List<T> nodes;

  public TreeBuilderUtils(List<T> nodes) {
    this.nodes = nodes;
  }

  /**
   * build a tree structure.
   */
  public List<T> buildTree() {
    List<T> treeNodes = Lists.newArrayList();
    List<T> rootNodes = getRootNodes();
    for (T rootNode : rootNodes) {
      buildChildNodes(rootNode);
      treeNodes.add(rootNode);
    }
    return treeNodes;
  }

  /**
   * recursive child nodes
   */
  public void buildChildNodes(T node) {
    List<T> children = getChildNodes(node);
    if (!children.isEmpty()) {
      for (T child : children) {
        buildChildNodes(child);
      }
      node.setNodes((List<Node>) children);
    }
  }

  /**
   * get all the children of the parent node
   */
  public List<T> getChildNodes(T parentNode) {
    List<T> childNodes = Lists.newArrayList();
    for (T n : nodes) {
      if (parentNode.getId() == n.getParentId()) {
        childNodes.add(n);
      }
    }
    return childNodes;
  }


  /**
   * is the root node?
   */
  public boolean rootNode(T node) {
    boolean isRootNode = true;
    for (T n : nodes) {
      if (node.getParentId() == n.getId()) {
        isRootNode = false;
        break;
      }
    }
    return isRootNode;
  }

  /**
   * get all root nodes of the list collection.
   */
  public List<T> getRootNodes() {
    List<T> rootNodes = Lists.newArrayList();
    for (T n : nodes) {
      if (rootNode(n)) {
        rootNodes.add(n);
      }
    }
    return rootNodes;
  }
}  
