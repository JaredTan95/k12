package cn.edu.cqjtu.k12.course.service;

import cn.edu.cqjtu.k12.course.domain.entity.CategoriesCourse;
import cn.edu.cqjtu.k12.course.domain.entity.Course;
import cn.edu.cqjtu.k12.course.exception.BadRequestException;
import cn.edu.cqjtu.k12.course.repository.CourseRepository;
import com.google.common.collect.Sets;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CourseService {

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private CategoriesCourseService categoriesCourseService;

    @Value("${k12.course.othersCategoryId}")
    private Integer OTHERS_CATEGORY_ID = 1;

    public void deleteAllByCategoryId(long id) {
        courseRepository.deleteAllByCategoryId(id);
    }

    /**
     * modifying the course to 'non-category'.
     */
    @Transactional
    public int modifyCategoryToOthers(List<Long> courseIds) {
        return courseRepository
                .updateCategoryToOthersByCourseIds(courseIds, Long.valueOf(OTHERS_CATEGORY_ID));
    }

    public Course getOne(long id) {
        return courseRepository.findOne(id);
    }

    /**
     * save a course with its category.
     */
    @Transactional
    public Course save(Course course) {
        CategoriesCourse categoriesCourse = new CategoriesCourse();

        Course entity = courseRepository.save(course);

        categoriesCourse.setCourseId(entity.getId());
        categoriesCourse.setCategoryId(course.getCategoryId());
        categoriesCourseService.save(categoriesCourse);
        return entity;
    }

    public Course update(Course course) {
        Course entity = courseRepository.findOne(course.getId());
        if (Objects.isNull(entity)) {
            throw new BadRequestException(String.format("Course id not exists. id = %s", course.getId()));
        }
        entity.setTitle(course.getTitle());
        entity.setDescription(course.getDescription());
        entity.setUrl(course.getUrl());
        entity.setVideoId(course.getVideoId());
        entity.setCover(course.getCover());
        entity.setCategoryId(course.getCategoryId());
        return courseRepository.save(entity);
    }

    public void deleteOne(Course course) {
        courseRepository.delete(course.getId());
    }

    public Course findPreCourse(long id) {
        long parentId = courseRepository.findOne(id).getParentId();
        return getOne(parentId);
    }

    public Course findNextCourse(long id) {
        return courseRepository.findByParentId(id);
    }

    /**
     * search course by title or description.
     */
    public Set<Course> searchCourse(Course course) {
        Set<Course> courseList = Sets.newLinkedHashSet();
        courseList.addAll(courseRepository.findAllByTitleContains(course.getTitle()));
        courseList.addAll(courseRepository.findAllByDescriptionContains(course.getDescription()));
        return courseList;
    }
}
