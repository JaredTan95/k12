package cn.edu.cqjtu.k12.course.repository;

import cn.edu.cqjtu.k12.course.domain.entity.Video;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface VideoRepository extends PagingAndSortingRepository<Video, Long> {

  List<Video> findByCategoryId(long categoryId);

  List<Video> findByTitleContains(String title);

  Video findByParentId(long parentId);

  List<Video> findAllByCourseId(long courseId);

  List<Video> findAllByTitle(String title);
}
