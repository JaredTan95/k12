package cn.edu.cqjtu.k12.course.repository;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring data jpa batch operation.
 */
@Component
public class EntityManagerRepository {

  @PersistenceContext
  protected EntityManager em;

  @Transactional
  public void batchInsert(List list) {
    int size = list.size();
    for (int i = 0; i < size; i++) {
      em.persist(list.get(i));
      if (i % 10 == 0) {
        em.flush();
        em.clear();
      }
    }
  }

  @Transactional
  public void batchUpdate(List list) {
    int size = list.size();
    for (int i = 0; i < size; i++) {
      em.merge(list.get(i));
      if (i % 10 == 0) {
        em.flush();
        em.clear();
      }
    }
  }
}
