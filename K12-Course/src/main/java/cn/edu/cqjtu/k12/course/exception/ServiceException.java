package cn.edu.cqjtu.k12.course.exception;

import org.springframework.http.HttpStatus;

public class ServiceException extends AbstractK12HttpException {

  public ServiceException(String str) {
    super(str);
    setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  public ServiceException(String str, Exception e) {
    super(str, e);
    setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
