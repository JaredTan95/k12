package cn.edu.cqjtu.k12.course;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
public class CourseServiceApplication {

  public static void main(String[] args) {
    new SpringApplicationBuilder(CourseServiceApplication.class).web(true).run(args);
  }

  /**
   * configuration Swagger Api.
   */
  @Configuration
  @EnableSwagger2
  public class SwaggerConfig {

    public static final String SWAGGER_SCAN_BASE_PACKAGE = "cn.edu.cqjtu.k12";
    public static final String VERSION = "0.0.1";

    ApiInfo apiInfo() {
      return new ApiInfoBuilder()
          .title("K12接口文档说明")
          .description("此接口文档仅供内部开发人员使用。 \n")
          .termsOfServiceUrl("")
          .version(VERSION)
          .contact(new Contact("Jared.Tan", "", "tanjian20150101@163.com"))
          .build();
    }

    @Bean
    public Docket customImplementation() {
      return new Docket(DocumentationType.SWAGGER_2)
          .select()
          .apis(RequestHandlerSelectors.basePackage(SWAGGER_SCAN_BASE_PACKAGE))
          .build()
          .apiInfo(apiInfo());
    }
  }

  @Bean
  public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
    return new PropertySourcesPlaceholderConfigurer();
  }

  /**
   * 设置上传文件大小，配置文件属性设置无效
   */
/*  @Bean
  public MultipartConfigElement multipartConfigElement() {
    MultipartConfigFactory config = new MultipartConfigFactory();
    config.setMaxFileSize("1000MB");
    config.setMaxRequestSize("700MB");
    return config.createMultipartConfig();
  }*/
}
