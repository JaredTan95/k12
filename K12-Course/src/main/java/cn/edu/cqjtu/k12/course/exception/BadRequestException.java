package cn.edu.cqjtu.k12.course.exception;


import org.springframework.http.HttpStatus;

public class BadRequestException extends AbstractK12HttpException {


  public BadRequestException(String str) {
    super(str);
    setHttpStatus(HttpStatus.BAD_REQUEST);
  }
}
