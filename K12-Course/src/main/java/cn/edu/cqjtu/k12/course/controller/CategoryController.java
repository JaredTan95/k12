package cn.edu.cqjtu.k12.course.controller;

import cn.edu.cqjtu.k12.course.domain.entity.Category;
import cn.edu.cqjtu.k12.course.domain.dto.CategoryDTO;
import cn.edu.cqjtu.k12.course.service.CategoryService;
import cn.edu.cqjtu.k12.course.service.CategoryService.DealCourseChoice;
import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

@Api(tags = "课程分类")
@RestController
@RequestMapping("/categories")
public class CategoryController {

  @Autowired
  private CategoryService categoryService;

  @ApiOperation(value = "获取课程分类", nickname = "getCategories"
      , notes = "无参数,将以树形结构返回所有分类")
  @GetMapping("")
  public DeferredResult<List<CategoryDTO>> getCategories() {
    DeferredResult<List<CategoryDTO>> defResult = new DeferredResult<>();
    /**
     * //TODO:try about using DeferredResult.
     */
    new Thread(() -> defResult.setResult(categoryService.findAll())).start();
    return defResult;
  }

  @ApiOperation(value = "添加一个分类", nickname = "addOne")
  @PostMapping("")
  public CategoryDTO addOne(@NotNull
  @ApiParam(value = "any value", required = true) @RequestParam("title") String title
      , @ApiParam(value = "父级分类ID,默认为0,代表顶层目录", required = true)
  @NotNull @RequestParam("parentId") long parentId) {
    Category category = new Category();
    category.setTitle(title);
    //TODO:make sure the operator.
    category.setParentId(parentId);
    category.setDataChangeCreatedBy("operator");
    return categoryService.save(category);
  }

  @ApiOperation(value = "更新一个分类", nickname = "updateOne")
  @PutMapping("/{id}")
  public CategoryDTO update(@PathVariable("id") long id, @RequestParam("title") String title,
      @RequestParam("description") String description, @RequestParam("parentId") long parentId) {
    Category category = new Category();
    category.setId(id);
    category.setTitle(title);
    category.setParentId(parentId);
    category.setDescription(description);
    //TODO:make sure the operator.
    category.setDataChangeLastModifiedBy("operator");
    return categoryService.updateById(category);
  }

  @ApiOperation(value = "删除一个分类,当该分类下面有所属课程时返回400.", nickname = "deleteOne")
  @DeleteMapping("/{id}/{method}")
  public List<CategoryDTO> delete(@PathVariable("id") long id,
      @ApiParam(value = "当该分类下有课程时对课程的可选操作方式，0(默认,提示异常无法删除),1(级联删除),2(设置为'其它')分类", required = true)
      @PathVariable("method") Integer method) {
    Category category = new Category();
    category.setId(id);
    List<CategoryDTO> categoryDTOS = Lists.newArrayList();

    switch (method) {
      case 1: {
        categoryDTOS.addAll(categoryService.deleteCategory(category, DealCourseChoice.DELETE));
      }
      break;
      case 2: {
        categoryDTOS.addAll(categoryService.deleteCategory(category, DealCourseChoice.OTHERS));
      }
      break;
      default:
        categoryDTOS.addAll(categoryService.deleteCategory(category, DealCourseChoice.DEFAULT));
        break;
    }
    return categoryDTOS;
  }

  @GetMapping("/{id}")
  public CategoryDTO getOne(@PathVariable("id") long id) {
    return categoryService.getOne(id);
  }
}
