package cn.edu.cqjtu.k12.course.repository;

import cn.edu.cqjtu.k12.course.domain.entity.Course;
import java.util.List;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CourseRepository extends PagingAndSortingRepository<Course, Long> {


  List<Course> findByCategoryId(long categoryId);

  List<Course> findByCategoryIdIn(List<Long> categoryIds);

  List<Course> findByVideoId(long videoId);

  List<Course> findAllByTitleContains(String title);

  List<Course> findAllByDescriptionContains(String description);

  void deleteAllByCategoryId(long categoryId);

  @Modifying
  @Query("UPDATE Course SET Category_Id = ?2 WHERE Id In ?1")
  int updateCategoryToOthersByCourseIds(List<Long> courseIds, Long categoryId);

  //@Modifying
  //@Query("SELECT T2.* FROM (SELECT @r AS _id,(SELECT @r := parent_id FROM course WHERE id = _id) AS parent_id, @l := @l + 1 AS lvl FROM (SELECT @r := ?1, @l := 0) vars,course h WHERE @r <> 0) T1 JOIN course T2 ON T1._id = T2.id ORDER BY T1.lvl DESC ")
  List<Course> findAllByParentId(long id);

  Course findByParentId(long parentId);
}
