package cn.edu.cqjtu.k12.course.repository;

import cn.edu.cqjtu.k12.course.domain.entity.CategoriesCourse;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CategoriesCourseRepository extends
    PagingAndSortingRepository<CategoriesCourse, Long> {

  List<CategoriesCourse> findAllByCategoryId(long categoryId);

  List<CategoriesCourse> findAllByCourseId(long courseId);

  List<CategoriesCourse> deleteByCategoryIdIn(List<Long> categoryIds);
}
