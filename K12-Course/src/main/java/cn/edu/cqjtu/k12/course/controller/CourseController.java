package cn.edu.cqjtu.k12.course.controller;

import cn.edu.cqjtu.k12.course.domain.entity.Course;
import cn.edu.cqjtu.k12.course.service.CourseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "课程")
@RestController
@RequestMapping("/course")
public class CourseController {

  @Autowired
  private CourseService courseService;

  @ApiOperation(value = "获取单个课程")
  @GetMapping("/{id}")
  public Course getOne(@ApiParam(value = "课程id", required = true)
  @PathVariable("id") long id) {
    return courseService.getOne(id);
  }

  @ApiOperation("添加一门课程")
  @PostMapping("")
  public Course add(@ApiParam(value = "Course实体", required = true)
  @RequestBody Course course) {
    return courseService.save(course);
  }

  @ApiOperation(value = "更新单个分类下的课程")
  @PutMapping("/{id}")
  public Course update(@PathVariable("id") long id, Course course) {
    return courseService.update(course);
  }

  @ApiOperation(value = "删除单个分类下的课程")
  @DeleteMapping("/{id}")
  public void delete(@PathVariable("id") long id) {
    Course course = new Course();
    course.setId(id);
    courseService.deleteOne(course);
  }

  /**
   * get the previous course.
   */
  @ApiOperation(value = "获取某个课程的上一课程")
  @GetMapping("/{id}/pre")
  public Course getPreviousCourse(@PathVariable("id") long id) {
    return courseService.findPreCourse(id);
  }

  /**
   * get the next course.
   */
  @ApiOperation(value = "获取某个课程的下一课程")
  @GetMapping("/{id}/next")
  public Course getNextCourse(@PathVariable("id") long id) {
    return courseService.findNextCourse(id);
  }

  /**
   * search course by search key.
   */
  @ApiOperation(value = "根据关键词搜索课程")
  @GetMapping("")
  public Set<Course> search(@RequestParam("q") String q) {
    Course course = new Course();
    course.setTitle(q);
    course.setDescription(q);
    return courseService.searchCourse(course);
  }
}
