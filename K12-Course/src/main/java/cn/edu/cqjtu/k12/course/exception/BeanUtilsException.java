package cn.edu.cqjtu.k12.course.exception;

public class BeanUtilsException extends RuntimeException {

  public BeanUtilsException(Throwable e) {
    super(e);
  }

}
