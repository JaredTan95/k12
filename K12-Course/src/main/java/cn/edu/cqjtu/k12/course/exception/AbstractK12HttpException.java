package cn.edu.cqjtu.k12.course.exception;

import org.springframework.http.HttpStatus;

public abstract class AbstractK12HttpException extends RuntimeException {

  private static final long serialVersionUID = -1713129594004951820L;

  protected HttpStatus httpStatus;

  public AbstractK12HttpException(String msg) {
    super(msg);
  }

  public AbstractK12HttpException(String msg, Exception e) {
    super(msg, e);
  }

  protected void setHttpStatus(HttpStatus httpStatus) {
    this.httpStatus = httpStatus;
  }

  public HttpStatus getHttpStatus() {
    return httpStatus;
  }
}
