package cn.edu.cqjtu.k12.course.controller;

import cn.edu.cqjtu.k12.course.domain.entity.Course;
import cn.edu.cqjtu.k12.course.domain.entity.Video;
import cn.edu.cqjtu.k12.course.service.VideoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "小节视频")
@RestController
@RequestMapping("/video")
public class VideoController {

  @Autowired
  private VideoService videoService;

  @ApiOperation("添加一节视频")
  @PostMapping("")
  public Video add(@RequestBody Video video) {
    return videoService.save(video);
  }

  @ApiOperation("获取节视频信息")
  @GetMapping("/{id}")
  public Video getOne(@PathVariable("id") long id) {
    return videoService.getOne(id);
  }

  @ApiOperation("获取某个视频的上一节视频")
  @GetMapping("/{id}/pre")
  public Video getPreviousVideo(@PathVariable("id") long id) {
    return videoService.getPreVideo(id);
  }

  @ApiOperation("获取某个视频的下一节视频")
  @GetMapping("/{id}/next")
  public Video getNextVideo(@PathVariable("id") long id) {
    return videoService.getNextVideo(id);
  }

  @ApiOperation("更新某节视频")
  @PutMapping("/{id}")
  public Video update(@PathVariable("id") long id, Video video) {
    return videoService.updateOne(video);
  }

  @ApiOperation("删除某节视频")
  @DeleteMapping("/{id}")
  public void delete(@PathVariable("id") long id) {
    videoService.deleteOne(id);
  }

  @GetMapping("/course/{courseId}")
  public List<Video> getByCourseId(@PathVariable("courseId") long courseId) {
    Course course = new Course();
    course.setId(courseId);
    return videoService.getByCourse(course);
  }
}

