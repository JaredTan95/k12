package cn.edu.cqjtu.k12.course.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "Course")
@SQLDelete(sql = "UPDATE Course SET Is_Deleted = 1 WHERE Id = ?")
@Where(clause = "Is_Deleted = 0")
public class Course extends BaseEntity {

    @Column(nullable = false)
    private String title;

    @Column(nullable = false, columnDefinition = "Int default '0'")
    private long categoryId;

    @Column(columnDefinition = "Int default '0'")
    private long videoId;

    @Column(columnDefinition = "varchar(255) default 'url'")
    private String url;

    @Column(columnDefinition = "varchar(255) default 'url'")
    private String cover;

    @Column(columnDefinition = "varchar(255) default 'description'")
    private String description;

    @Column(columnDefinition = "Int default '0'")
    private long parentId;

    @Column(columnDefinition = "Int default '0'")
    private long duration;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public long getVideoId() {
        return videoId;
    }

    public void setVideoId(long videoId) {
        this.videoId = videoId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }
}
