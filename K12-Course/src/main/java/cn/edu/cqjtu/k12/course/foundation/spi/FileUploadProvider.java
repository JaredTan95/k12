package cn.edu.cqjtu.k12.course.foundation.spi;

import cn.edu.cqjtu.k12.course.domain.dto.UploadResponseDTO;
import cn.edu.cqjtu.k12.course.foundation.internals.QiniuUploaderProperty;
import java.io.InputStream;

public interface FileUploadProvider {

  /**
   * upload by file or file path.
   */
  UploadResponseDTO uploadByFile(QiniuUploaderProperty property, String filePath);

  /**
   * upload by input stream.
   */
  UploadResponseDTO uploadByInputStream(QiniuUploaderProperty property, InputStream stream);


  /**
   * upload by bytes.
   */
  UploadResponseDTO uploadByBytes(QiniuUploaderProperty property, byte[] data);
}
