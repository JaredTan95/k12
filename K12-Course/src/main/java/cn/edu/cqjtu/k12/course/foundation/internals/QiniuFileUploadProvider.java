package cn.edu.cqjtu.k12.course.foundation.internals;

import cn.edu.cqjtu.k12.course.domain.dto.UploadResponseDTO;
import cn.edu.cqjtu.k12.course.foundation.spi.FileUploadProvider;
import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QiniuFileUploadProvider implements FileUploadProvider {

  private static final Logger logger = LoggerFactory.getLogger(QiniuUploaderProperty.class);

  private Configuration configuration;

  private UploadManager uploadManager;

  public QiniuFileUploadProvider() {
    configuration = new Configuration(Zone.zone0());
    uploadManager = new UploadManager(configuration);
  }

  @Override
  public UploadResponseDTO uploadByFile(QiniuUploaderProperty property, String filePath) {
    configuration = new Configuration(property.getZone());
    //默认不指定key的情况下，以文件内容的hash值作为文件名
    String key = property.getFileName();
    UploadResponseDTO responseDTO = new UploadResponseDTO();
    Auth auth = Auth.create(property.getAccessKey(), property.getSecretKey());
    String upToken = auth.uploadToken(property.getBucket());
    try {
      Response response = uploadManager.put(filePath, key, upToken);
      //解析上传成功的结果
      DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
      responseDTO.setAddress(response.address).setStatusCode(response.statusCode)
          .setDuration(response.duration).setError(response.error).setKey(putRet.key)
          .setHash(putRet.hash);
      logger.info("key:{} ,hash:{}", putRet.key, putRet.hash);
    } catch (QiniuException ex) {
      responseDTO.setError(ex.response.error);
      logger.error(ex.response.error);
    }
    return responseDTO;
  }

  @Override
  public UploadResponseDTO uploadByInputStream(QiniuUploaderProperty property, InputStream stream) {
    configuration = new Configuration(property.getZone());
    String key = property.getFileName();
    UploadResponseDTO responseDTO = new UploadResponseDTO();
    Auth auth = Auth.create(property.getAccessKey(), property.getSecretKey());
    String upToken = auth.uploadToken(property.getBucket());
    try {
      Response response = uploadManager.put(stream, key, upToken, null, null);
      DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
      responseDTO.setAddress(response.address).setStatusCode(response.statusCode)
          .setDuration(response.duration).setError(response.error).setKey(putRet.key)
          .setHash(putRet.hash);
      logger.info("key:{} ,hash:{}", putRet.key, putRet.hash);
    } catch (QiniuException ex) {
      responseDTO.setError(ex.response.error);
      logger.error(ex.response.error);
    }
    return responseDTO;
  }

  @Override
  public UploadResponseDTO uploadByBytes(QiniuUploaderProperty property, byte[] data) {
    configuration = new Configuration(property.getZone());
    String key = property.getFileName();
    UploadResponseDTO responseDTO = new UploadResponseDTO();
    Auth auth = Auth.create(property.getAccessKey(), property.getSecretKey());
    String upToken = auth.uploadToken(property.getBucket());
    try {
      Response response = uploadManager.put(data, key, upToken);
      DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
      responseDTO.setAddress(response.address).setStatusCode(response.statusCode)
          .setDuration(response.duration).setError(response.error).setKey(putRet.key)
          .setHash(putRet.hash);
      logger.info("key:{} ,hash:{}", putRet.key, putRet.hash);
    } catch (QiniuException ex) {
      responseDTO.setError(ex.response.error);
      logger.error(ex.response.error);
    }
    return responseDTO;
  }
}
