package cn.edu.cqjtu.k12.course.domain.converter;

import cn.edu.cqjtu.k12.course.domain.entity.Category;
import cn.edu.cqjtu.k12.course.domain.dto.CategoryDTO;
import com.google.common.base.Converter;
import org.springframework.stereotype.Component;

/**
 * Converter for transforms between category and categoryDTO.
 */
@Component
public class CategoryConverter extends Converter<Category, CategoryDTO> {

  @Override
  protected CategoryDTO doForward(Category category) {

    return new CategoryDTO(category.getId(), category.isDeleted(), category.getTitle(),
        category.getParentId()
        , category.getDescription(), category.getDataChangeCreatedBy(),
        category.getDataChangeCreatedTime(), category.getDataChangeLastModifiedBy()
        , category.getDataChangeLastModifiedTime());
  }

  @Override
  protected Category doBackward(CategoryDTO categoryDTO) {
    return new Category(categoryDTO.getId(), categoryDTO.isDeleted(),
        categoryDTO.getTitle(), categoryDTO.getParentId()
        , categoryDTO.getDescription(), categoryDTO.getDataChangeCreatedBy(),
        categoryDTO.getDataChangeCreatedTime(), categoryDTO.getDataChangeLastModifiedBy()
        , categoryDTO.getDataChangeLastModifiedTime());
  }
}
