INSERT INTO `Category` (`Id`, `Title`, `Parent_Id`, `Description`, `Data_Change_Created_By`) VALUES
  (60, 'IT互联网', 0, '0', 'admin'),
  (61, '移动开发', 60, '0-60', 'admin'),
  (65, '办公技能', 64, '0-64', 'admin'),
  (62, '编程语言', 60, '0-60', 'admin'),
  (63, '产品设计', 60, '0-60', 'admin'),
  (64, '职场技能', 0, '0', 'admin'),
  (66, '职业考试', 64, '0-64', 'admin'),
  (67, '人力资源', 64, '0-64', 'admin'),
  (68, '语言学习', 0, '0', 'admin'),
  (69, '实用英语', 68, '0-68', 'admin'),
  (70, '韩语', 68, '0-68', 'admin'),
  (71, '托福雅思', 68, '0-68', 'admin'),
  (72, '兴趣爱好', 0, '0', 'admin'),
  (73, '唱歌', 72, '0-72', 'admin'),
  (74, '跳舞', 72, '0-72', 'admin'),
  (75, '摄影', 72, '0-72', 'admin'),
  (76, '更多分类', 0, '0', 'admin'),
  (77, '育儿', 76, '0-76', 'admin'),
  (78, '中学', 76, '0-76', 'admin'),
  (79, '大学', 76, '0-76', 'admin'),
  (1, '其它', 0, '0-76', 'admin');

INSERT INTO `Course` (`Id`, `Category_Id`, `Title`, `Parent_Id`
  , `Data_Change_Created_By`, `Video_Id`, `Cover`, `url`) VALUES
  (1, 60, '测试一', 0, 'tanjian', 1, 'no cover', 'http://url'),
  (2, 70, '测试2', 1, 'tanjian', 1, 'no cover', 'http://url'),
  (3, 70, '测试3', 2, 'tanjian', 2, 'no cover', 'http://url');

INSERT INTO `Video` (`Id`, `Title`, `Url`, `Cover`, `Course_Id`,
                     `Category_Id`, `Data_Change_Created_By`, `Parent_Id`) VALUES
  (1, 'no title', 'no url', 'no cover', 1, 60, 'no name', 0),
  (2, 'no title2', 'no url2', 'no cover2', 1, 60, 'no name', 1),
  (3, 'no title3', 'no url3', 'no cover3', 1, 60, 'no name', 2);

INSERT INTO `Categories_Course` (`Course_Id`, `Category_Id`, `Data_Change_Created_By`) VALUES
  (1, 60, 'Tan');
