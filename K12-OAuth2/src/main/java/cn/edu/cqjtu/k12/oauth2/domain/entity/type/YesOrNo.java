package cn.edu.cqjtu.k12.oauth2.domain.entity.type;

public enum YesOrNo {
    Y(true), N(false);

    private boolean status;

    YesOrNo(boolean status) {
        this.status = status;
    }
}
