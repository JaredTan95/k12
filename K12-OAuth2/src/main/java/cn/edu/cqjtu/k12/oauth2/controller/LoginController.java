package cn.edu.cqjtu.k12.oauth2.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class LoginController {

    @GetMapping("/authentication/require")
    public ModelAndView require() {
        return new ModelAndView("ftl/login");
    }

    @GetMapping("/login/qq")
    public String loginWithQq() {
        return "/login/qq";
    }

    @GetMapping("/login/weixin")
    public String loginWithWeChat() {
        return "/login/weixin";
    }

    @GetMapping("/login/weibo")
    public String loginWithWeiBo() {
        return "/login/weibo";
    }
}
