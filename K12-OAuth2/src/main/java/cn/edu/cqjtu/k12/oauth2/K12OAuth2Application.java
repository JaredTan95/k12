package cn.edu.cqjtu.k12.oauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;

@SpringBootApplication
@EnableOAuth2Sso
public class K12OAuth2Application {
    public static void main(String[] args) {
        SpringApplication.run(K12OAuth2Application.class, args);
    }
}
