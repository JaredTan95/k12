package cn.edu.cqjtu.k12.oauth2.repository;

import cn.edu.cqjtu.k12.oauth2.domain.entity.RoleEntity;
import cn.edu.cqjtu.k12.oauth2.domain.entity.type.Authority;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface RoleEntityRepository extends PagingAndSortingRepository<RoleEntity, Long> {
    List<RoleEntity> findByRole(Authority auth);
}
