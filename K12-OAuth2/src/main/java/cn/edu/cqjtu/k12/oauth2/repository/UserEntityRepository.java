package cn.edu.cqjtu.k12.oauth2.repository;

import cn.edu.cqjtu.k12.oauth2.domain.entity.UserEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserEntityRepository extends PagingAndSortingRepository<UserEntity, Long> {
    Optional<UserEntity> findByUsername(String username);

    Optional<List<UserEntity>> findAllByNickNameContains(String nickname, Pageable pageable);
}
