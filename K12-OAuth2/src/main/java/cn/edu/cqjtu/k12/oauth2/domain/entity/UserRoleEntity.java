package cn.edu.cqjtu.k12.oauth2.domain.entity;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Tolerate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@Entity
@Table(name = "User_Role")
@SQLDelete(sql = "UPDATE User_Role SET Is_Deleted = 1 WHERE Id = ?")
@Where(clause = "Is_Deleted = 0")
public class UserRoleEntity extends BaseEntity {
    @Column(name = "user_id")
    private long userId;

    @Column(name = "role_id")
    private long roleId;

    @Tolerate
    public UserRoleEntity() {

    }
}
