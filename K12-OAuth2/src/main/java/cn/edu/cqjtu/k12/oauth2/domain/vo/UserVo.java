package cn.edu.cqjtu.k12.oauth2.domain.vo;

import cn.edu.cqjtu.k12.oauth2.domain.entity.type.Gender;
import lombok.Builder;
import lombok.Data;

import java.util.Date;


@Data
@Builder
public class UserVo {

    private Long id;

    private String phone;

    private String username;

    private String password;

    private String avatar;

    private String salt;

    private String nickName;

    private Date birthday;

    private Gender sex;

    private Integer age;

    private String email;

    private String address;
}
