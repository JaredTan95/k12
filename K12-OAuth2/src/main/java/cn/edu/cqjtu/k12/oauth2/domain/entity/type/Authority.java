package cn.edu.cqjtu.k12.oauth2.domain.entity.type;

public enum Authority {
    STUDENT, TEACHER, EDU, ADMIN
}
