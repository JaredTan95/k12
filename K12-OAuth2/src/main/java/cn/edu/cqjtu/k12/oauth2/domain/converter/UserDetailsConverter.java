package cn.edu.cqjtu.k12.oauth2.domain.converter;

import cn.edu.cqjtu.k12.oauth2.domain.K12UserDetails;
import cn.edu.cqjtu.k12.oauth2.domain.entity.RoleEntity;
import cn.edu.cqjtu.k12.oauth2.domain.entity.UserEntity;
import cn.edu.cqjtu.k12.oauth2.domain.entity.type.Authority;
import com.google.common.base.Converter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserDetailsConverter extends Converter<UserEntity, K12UserDetails> {
    @Override
    protected K12UserDetails doForward(UserEntity entity) {
        return K12UserDetails.builder().username(entity.getUsername()).avatar(entity.getAvatar())
                .password(entity.getPassword()).salt(entity.getSalt()).nickName(entity.getNickName())
                .birthday(entity.getBirthday()).sex(entity.getSex())
                .age(entity.getAge()).address(entity.getAddress()).authorities(mapToGrantedAuthorities(entity.getRoles())).email(entity.getEmail())
                .phone(entity.getPhone()).build();
    }

    @Override
    protected UserEntity doBackward(K12UserDetails userDetails) {
        return UserEntity.builder().avatar(userDetails.getAvatar())
                .username(userDetails.getUsername())
                .password(userDetails.getPassword())
                .salt(userDetails.getSalt())
                .nickName(userDetails.getNickName())
                .birthday(userDetails.getBirthday())
                .sex(userDetails.getSex())
                .address(userDetails.getAddress())
                .age(userDetails.getAge()).roles(mapToRoles(userDetails.getAuthorities())).email(userDetails.getEmail())
                .phone(userDetails.getPhone()).build();
    }


    private static List<GrantedAuthority> mapToGrantedAuthorities(List<RoleEntity> roles) {
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getRole().name()))
                .collect(Collectors.toList());
    }

    private static List<RoleEntity> mapToRoles(Collection<? extends GrantedAuthority> authorities) {
        return authorities.stream().map(item -> RoleEntity.builder()
                .role(Authority.valueOf(item.getAuthority())).build())
                .collect(Collectors.toList());
    }
}
