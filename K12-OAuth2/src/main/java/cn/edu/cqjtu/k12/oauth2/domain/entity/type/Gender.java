package cn.edu.cqjtu.k12.oauth2.domain.entity.type;

import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum Gender {
    MALE(1),
    FEMALE(2),
    NONE(0);

    private Integer value;

    private static final Map<Integer, Gender> ENUM_MAP;

    Gender(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }

    static {
        Map<Integer, Gender> map = new ConcurrentHashMap<>();
        for (Gender instance : Gender.values()) {
            map.put(instance.getValue(), instance);
        }
        ENUM_MAP = Collections.unmodifiableMap(map);
    }

    public static Gender get(Integer name) {
        if (StringUtils.isEmpty(name)) {
            return null;
        }
        return ENUM_MAP.get(name);
    }
}
