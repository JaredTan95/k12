package cn.edu.cqjtu.k12.oauth2.service;

import cn.edu.cqjtu.k12.oauth2.domain.K12UserDetails;
import cn.edu.cqjtu.k12.oauth2.domain.converter.UserDetailsConverter;
import cn.edu.cqjtu.k12.oauth2.domain.entity.UserEntity;
import cn.edu.cqjtu.k12.oauth2.exception.UsernameNotFoundException;
import cn.edu.cqjtu.k12.oauth2.repository.UserEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserDetailsConverter userDetailsConverter;

    @Autowired
    private UserEntityRepository userRepository;

    public UserEntity findByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() ->
                        new UsernameNotFoundException(String.format("No user found with username '%s'.", username)));
    }

    public List<UserEntity> getUserByNickName(String nickName, Pageable pageable) {
        return userRepository.findAllByNickNameContains(nickName, pageable)
                .orElseThrow(() ->
                        new UsernameNotFoundException(String.format("No user found with nickName '%s' in page: '%s'."
                                , nickName, pageable.getPageNumber())));
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws org.springframework.security.core.userdetails.UsernameNotFoundException {

        UserEntity entity = this.findByUsername(s);
        return userDetailsConverter.convert(this.findByUsername(s));
    }

    public UserEntity addOneUser(K12UserDetails userDetails) {

        return null;
    }
}
