package cn.edu.cqjtu.k12.oauth2.domain.entity;

import cn.edu.cqjtu.k12.oauth2.domain.entity.type.Gender;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Tolerate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;


@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "User")
@SQLDelete(sql = "UPDATE User SET Is_Deleted = 1 WHERE Id = ?")
@Where(clause = "Is_Deleted = 0")
public class UserEntity extends BaseEntity {
    @Column
    private String avatar;

    @Column
    private String username;

    @Column
    private String password;

    @Column
    private String salt;

    @Column
    private String nickName;

    @Column
    private Date birthday;

    @Column(columnDefinition = "Int default '0'")
    @Enumerated(EnumType.ORDINAL)
    private Gender sex;

    @Column(columnDefinition = "Int default '0'")
    private int age;

    @Column
    private String email;

    @Column
    private String address;

    @Column
    private String phone;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "User_Role",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")})
    private List<RoleEntity> roles;

    @Tolerate
    public UserEntity() {

    }
}
