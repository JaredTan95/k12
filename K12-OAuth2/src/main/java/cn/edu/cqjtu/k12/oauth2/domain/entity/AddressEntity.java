package cn.edu.cqjtu.k12.oauth2.domain.entity;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Tolerate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "Address")
@SQLDelete(sql = "UPDATE Address SET Is_Deleted = 1 WHERE Id = ?")
@Where(clause = "Is_Deleted = 0")
public class AddressEntity extends BaseEntity {
    @Column
    private Long userId;

    @Column
    private String address;

    @Tolerate
    public AddressEntity() {

    }
}
