package cn.edu.cqjtu.k12.oauth2.repository;

import cn.edu.cqjtu.k12.oauth2.domain.entity.AddressEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AdressEntityRepository extends PagingAndSortingRepository<AddressEntity, Long> {
    List<AddressEntity> findAllByUserId(Long userId, Pageable pageable);
}
