package cn.edu.cqjtu.k12.oauth2.domain.entity;

import cn.edu.cqjtu.k12.oauth2.domain.entity.type.Authority;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Tolerate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "Role")
@SQLDelete(sql = "UPDATE Role SET Is_Deleted = 1 WHERE Id = ?")
@Where(clause = "Is_Deleted = 0")
public class RoleEntity extends BaseEntity {

    @Column
    @Enumerated(EnumType.STRING)
    private Authority role;

    @Tolerate
    public RoleEntity() {

    }
}
